using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class my : MonoBehaviour
{
    public GameObject Mycube;
    public int transSpeed = 100;
    public float rotaSpeed = 10.5f;
    public float scale = 3;
    void OnGUI()
    {
        if (Input.GetKey(KeyCode.Z))
        {
            Mycube.transform.Translate(Vector3.forward * transSpeed * Time.deltaTime, Space.World);
        }
        if (Input.GetKey(KeyCode.X))
        {
            Mycube.transform.Rotate(Vector3.up*rotaSpeed, Space.World);
        }
        if (Input.GetKey(KeyCode.C))
        {
            Mycube.transform.localScale = new Vector3(scale, scale, scale);
        }
        if (Input.GetKey(KeyCode.V))
        {
            Mycube.transform.Translate(-Vector3.forward * transSpeed * Time.deltaTime, Space.World);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
