using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    Transform m_camTransform;
    Vector3 m_camRot;
    float m_camHeight = 1.4f;

    public Transform m_transform;
    CharacterController m_ch;                    //角色控制器组件
    float m_movSpeed = 3.0f;
    float m_gravity = 2.0f;
    // Start is called before the first frame update
    void Start()
    {
        m_transform = this.transform;
        m_ch = this.GetComponent<CharacterController>();   //获取控制组件
        m_camTransform = Camera.main.transform;            //获取摄像机
        Vector3 pos = m_camTransform.position;
        pos.y += m_camHeight;
        m_camTransform.position = pos;
        m_camTransform.rotation = m_transform.rotation;
        m_camRot = m_camTransform.eulerAngles;
        Screen.lockCursor = true;
    }

    // Update is called once per frame
    void Update()
    {
        Control();
    }

    void Control()
    {
        float xm = 0, ym = 0, zm = 0;
        float rh = Input.GetAxis("Mouse X");
        float rv = Input.GetAxis("Mouse Y");

        m_camRot.x -= rv;
        m_camRot.y += rh;
        m_camTransform.eulerAngles = m_camRot;
        Vector3 camrot = m_camTransform.eulerAngles;
        camrot.x = 0;
        camrot.z = 0;
        m_transform.eulerAngles = camrot;
        ym -= m_gravity * Time.deltaTime;
        if (Input.GetKey(KeyCode.W))
        { 
            zm += m_movSpeed * Time.deltaTime;
        } else if (Input.GetKey(KeyCode.S))
        {
            zm -= m_movSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D))
        {
            xm += m_movSpeed * Time.deltaTime;
        } else if (Input.GetKey(KeyCode.A))
        {
            xm -= m_movSpeed * Time.deltaTime;
        }

        m_ch.Move(m_transform.TransformDirection(new Vector3(xm, ym, zm)));
        Vector3 pos = m_transform.position;
        pos.y += m_camHeight;
        m_camTransform.position = pos;
    }
}
